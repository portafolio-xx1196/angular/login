import {Injectable} from '@angular/core';
import {LoginRequestI} from "./interfaces/LoginRequestI";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {BehaviorSubject, catchError, Observable, tap, throwError} from "rxjs";
import {UserI} from "./interfaces/UserI";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  currentUserLoginOn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
  currentUserData: BehaviorSubject<UserI> = new BehaviorSubject<UserI>({
    id: 0,
    name: "",
    lastname: "",
    email: ""
  })

  constructor(private httpClient: HttpClient) {
  }

  login(credentials: LoginRequestI): Observable<UserI> {
    return this.httpClient.get<UserI>("././assets/data.json").pipe(
      tap(userData => {
        this.currentUserData.next(userData)
        this.currentUserLoginOn.next(true)
      }),
      catchError(this.handlerError)
    )
  }

  private handlerError(httpErrorResponse: HttpErrorResponse) {
    if (httpErrorResponse.status === 0) {
      console.log("Se ha producido un error ", httpErrorResponse.status)
    } else {
      console.log("Error en backend ", httpErrorResponse.status, httpErrorResponse.error)
    }
    return throwError(() => new Error("Intente neuvamente"))
  }

  get userData(): Observable<UserI> {
    return this.currentUserData.asObservable()
  }

  get userLoginOn(): Observable<boolean> {
    return this.currentUserLoginOn.asObservable()
  }
}
