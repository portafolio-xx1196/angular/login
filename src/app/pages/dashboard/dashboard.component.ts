import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavComponent} from "../../shared/nav/nav.component";
import {NgIf} from "@angular/common";
import {LoginService} from "../../services/auth/login.service";
import {UserI} from "../../services/auth/interfaces/UserI";

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    NavComponent,
    NgIf
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent implements OnInit, OnDestroy {
  userLoginOn: boolean = false;
  userData?: UserI;

  constructor(private loginService: LoginService) {
  }

  ngOnDestroy(): void {
    this.loginService.currentUserLoginOn.unsubscribe()
    this.loginService.currentUserData.unsubscribe()
  }

  ngOnInit(): void {
    this.loginService.currentUserLoginOn.subscribe({
      next: (userLoginOn) => {
        this.userLoginOn = userLoginOn
      }
    })

    this.loginService.currentUserData.subscribe({
      next: (userData) => {
        this.userData = userData
      }
    })
  }
}
