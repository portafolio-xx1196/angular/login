import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgIf} from "@angular/common";
import {RouterLink} from "@angular/router";
import {LoginService} from "../../services/auth/login.service";

@Component({
  selector: 'app-nav',
  standalone: true,
  imports: [
    NgIf,
    RouterLink
  ],
  templateUrl: './nav.component.html',
  styleUrl: './nav.component.scss'
})
export class NavComponent implements OnInit, OnDestroy {
  userLoginOn: boolean = false;

  constructor(private loginService: LoginService) {
  }

  ngOnDestroy(): void {
    this.loginService.currentUserLoginOn.unsubscribe()
    this.loginService.currentUserData.unsubscribe()
  }

  ngOnInit(): void {
    this.loginService.currentUserLoginOn.subscribe({
      next: (userLoginOn) => {
        this.userLoginOn = userLoginOn
      }
    })
  }
}
