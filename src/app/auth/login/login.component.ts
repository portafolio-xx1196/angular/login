import {Component} from '@angular/core';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginService} from "../../services/auth/login.service";
import {NgIf} from "@angular/common";
import {LoginRequestI} from "../../services/auth/interfaces/LoginRequestI";
import {routes} from "../../app.routes";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgIf
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  loginError: string = "";
  loginForm = this.formBuilder.group({
    email: ['iva@gmail.com', [Validators.required, Validators.email]],
    password: ['', Validators.required],
  })

  constructor(private formBuilder: FormBuilder, private router: Router, private loginService: LoginService) {
  }


  get email() {
    return this.loginForm.controls.email;
  }

  get password() {
    return this.loginForm.controls.password;
  }

  login() {
    if (this.loginForm.valid) {
      this.loginError = "";
      this.loginService.login(this.loginForm.value as LoginRequestI).subscribe({
        next:(userData) => {
          console.log(userData)
        },
        error:(errorData) =>{
          console.log(errorData)
        },
        complete:()=>{
          console.log("complete!!!!!!!!!!!")
          this.router.navigateByUrl("/home")
          this.loginForm.reset()
        }
      })
    } else {
      this.loginForm.markAllAsTouched();
      alert("Error al ingresar los datos.");
    }
  }

}
