export interface UserI {
  id: number
  name?: string
  lastname?: string
  email?: string
}
